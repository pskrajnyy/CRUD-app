package com.crud.tasks.mapper;

import com.crud.tasks.domain.TrelloBoard;
import com.crud.tasks.domain.TrelloCard;
import com.crud.tasks.domain.TrelloList;
import com.crud.tasks.dto.TrelloBoardDto;
import com.crud.tasks.dto.TrelloCardDto;
import com.crud.tasks.dto.TrelloListDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrelloMapperTest {

    @Autowired
    private TrelloMapper trelloMapper;

    @Test
    public void mapToBoards() {
        //Given
        TrelloListDto trelloListDto = new TrelloListDto("id", "name", true);
        TrelloListDto trelloListDto1 = new TrelloListDto("id1", "name1", false);
        List<TrelloListDto> trelloListDtos = new ArrayList<>();
        trelloListDtos.add(trelloListDto);
        trelloListDtos.add(trelloListDto1);
        TrelloBoardDto trelloBoardDto = new TrelloBoardDto("id", "name", trelloListDtos);
        TrelloBoardDto trelloBoardDto1 = new TrelloBoardDto("id1", "name1", trelloListDtos);
        List<TrelloBoardDto> trelloBoardDtos = new ArrayList<>();
        trelloBoardDtos.add(trelloBoardDto);
        trelloBoardDtos.add(trelloBoardDto1);

        //When
        List<TrelloBoard> trelloBoards = trelloMapper.mapToBoards(trelloBoardDtos);

        //Then
        Assert.assertEquals(2, trelloBoards.size());
    }

    @Test
    public void mapToBoardsDto() {
        //Given
        TrelloList trelloList = new TrelloList("id", "name", true);
        TrelloList trelloList1 = new TrelloList("id1", "name1", false);
        List<TrelloList> trelloLists = new ArrayList<>();
        trelloLists.add(trelloList);
        trelloLists.add(trelloList1);
        TrelloBoard trelloBoard = new TrelloBoard("id", "name", trelloLists);
        TrelloBoard trelloBoard1 = new TrelloBoard("id1", "name1", trelloLists);
        List<TrelloBoard> trelloBoards = new ArrayList<>();
        trelloBoards.add(trelloBoard);
        trelloBoards.add(trelloBoard1);

        //When
        List<TrelloBoardDto> trelloBoardsDto = trelloMapper.mapToBoardsDto(trelloBoards);

        //Then
        Assert.assertEquals(2, trelloBoardsDto.size());
    }

    @Test
    public void mapToList() {
        //Given
        TrelloListDto trelloListDto = new TrelloListDto("id", "name", true);
        TrelloListDto trelloListDto1 = new TrelloListDto("id1", "name1", false);
        List<TrelloListDto> trelloListsDto = new ArrayList<>();
        trelloListsDto.add(trelloListDto);
        trelloListsDto.add(trelloListDto1);

        //When
        List<TrelloList> trelloLists = trelloMapper.mapToLists(trelloListsDto);

        //Then
        Assert.assertEquals(2, trelloLists.size());

        Assert.assertEquals("id", trelloLists.get(0).getId());
        Assert.assertEquals("name", trelloLists.get(0).getName());
        Assert.assertEquals(true, trelloLists.get(0).getClosed());

        Assert.assertEquals("id1", trelloLists.get(1).getId());
        Assert.assertEquals("name1", trelloLists.get(1).getName());
        Assert.assertEquals(false, trelloLists.get(1).getClosed());
    }

    @Test
    public void mapToListDto() {
        //Given
        TrelloList trelloList = new TrelloList("id", "name", true);
        TrelloList trelloList1 = new TrelloList("id1", "name1", false);
        List<TrelloList> trelloLists = new ArrayList<>();
        trelloLists.add(trelloList);
        trelloLists.add(trelloList1);

        //When
        List<TrelloListDto> trelloListsDto = trelloMapper.mapToListsDto(trelloLists);

        //Then
        Assert.assertEquals(2, trelloListsDto.size());

        Assert.assertEquals("id", trelloListsDto.get(0).getId());
        Assert.assertEquals("name", trelloListsDto.get(0).getName());
        Assert.assertEquals(true, trelloListsDto.get(0).getClosed());

        Assert.assertEquals("id1", trelloListsDto.get(1).getId());
        Assert.assertEquals("name1", trelloListsDto.get(1).getName());
        Assert.assertEquals(false, trelloListsDto.get(1).getClosed());
    }

    @Test
    public void mapToCardDto() {
        //Given
        TrelloCard trelloCard = new TrelloCard("name", "description", "pos", "listId");

        //When
        TrelloCardDto trelloCardDto = trelloMapper.mapToCardDto(trelloCard);

        //Then
        Assert.assertEquals("name", trelloCardDto.getName());
        Assert.assertEquals("description", trelloCardDto.getDescription());
        Assert.assertEquals("pos", trelloCardDto.getPos());
        Assert.assertEquals("listId", trelloCardDto.getListId());
    }

    @Test
    public void mapToCard() {
        //Given
        TrelloCardDto trelloCardDto = new TrelloCardDto("name", "description", "pos", "listId");

        //When
        TrelloCard trelloCard = trelloMapper.mapToCard(trelloCardDto);

        //Then
        Assert.assertEquals("name", trelloCard.getName());
        Assert.assertEquals("description", trelloCard.getDescription());
        Assert.assertEquals("pos", trelloCard.getPos());
        Assert.assertEquals("listId", trelloCard.getListId());
    }
}
