package com.crud.tasks.mapper;

import com.crud.tasks.domain.Task;
import com.crud.tasks.dto.TaskDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskMapperTest {

    @Autowired
    private TaskMapper taskMapper;

    @Test
    public void mapToTaskDto() {
        //Given
        Task task = new Task((long) 1, "Title", "content");

        //When
        TaskDto taskDto = taskMapper.mapToTaskDto(task);

        //Then
        Assert.assertEquals(task.getId(), taskDto.getId());
        Assert.assertEquals(task.getTitle(), taskDto.getTitle());
        Assert.assertEquals(task.getContent(), taskDto.getContent());
    }

    @Test
    public void mapToTask() {
        //Given
        TaskDto taskDto = new TaskDto((long) 1, "Title", "content");

        //When
        Task task = taskMapper.mapToTask(taskDto);

        //Then
        Assert.assertEquals(taskDto.getId(), task.getId());
        Assert.assertEquals(taskDto.getTitle(), task.getTitle());
        Assert.assertEquals(taskDto.getContent(), task.getContent());
    }

    @Test
    public void mapToTaskDtoList() {
        //Given
        Task task = new Task((long) 1, "Title", "content");
        Task task1 = new Task((long) 2, "Title1", "content");
        Task task2 = new Task((long) 3, "Title2", "content");
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        tasks.add(task1);
        tasks.add(task2);

        //When
        List<TaskDto> taskDtos = taskMapper.mapToTaskDtoList(tasks);

        //Then
        Assert.assertEquals(3, taskDtos.size());

    }
}
